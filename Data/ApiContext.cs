using Microsoft.EntityFrameworkCore;
using fileRPG.Models;
using fileRPG.Helpers;

namespace fileRPG.Data
{
  public class ApiContext : DbContext
  {
    public ApiContext(DbContextOptions<ApiContext> options)
      : base(options)
    {}
    
    public DbSet<fileModel> fileModel { get; set; }
    public DbSet<userModel> userModel { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      // initial user. password must be changed later.
      modelBuilder.Entity<userModel>()
        .HasData(new userModel { Id = 1, Username = "Mestre", Password = AuthenticationHelper.ComputeHash("123"), 
        Role = fileRPG.Models.userModel.RoleEnum.admin.ToString() });

      // set uUsername as Unique
      modelBuilder.Entity<userModel>()
        .HasIndex(u => u.Username)
        .IsUnique();
    }
  }
}