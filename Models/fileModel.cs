using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace fileRPG.Models
{
    public class fileModel
    {
        public enum PriorityEnum
        {
            Guerreiro,
            Arqueiro,
            Mago    
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Power { get; set; }
        public string Weapon { get; set; }
        public string Race { get; set; }
        public PriorityEnum Class { get; set; }
    }
}